## Dev Quick Start

> Please ensure that `npm` is installed on your device before proceeding. For all quick commands please consult the `scripts` section of `package.json`


To start development on the project open up a terminal/command prompt at the root folder of this project (the folder where `package.json` resides) and enter:

```
npm run start
```
this will get the development server up and running on `localhost:8080`


## Production Build

To compile all code and bundle the assets for deployment use the command:
```
npm run build:prod
```