var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require("copy-webpack-plugin");
/**
 * UNCOMMENT LINES BELOW TO CREATE MODULAR PACKAGES TO LAZY LOAD ON DEMAND
 */
// var webpack = require('webpack');

module.exports = function () {
    return {
        entry: [path.resolve(__dirname, '../src/app')],
        // entry: {index: "./src/app", client: "./src/client"},
        output: {
            path: path.resolve(__dirname, '../dist'),
            filename: '[name].meetman.js'
        },
        resolve: {
            extensions: ['.js', '.ts', '.tsx']
        },
        module: {
            rules: [{
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    loaders: ['awesome-typescript-loader']
                },
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract({
                        fallbackLoader: "style-loader",
                        loader: "css-loader!sass-loader",
                    }),
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, '../src/index.html')
            }),
            new ExtractTextPlugin("styles.css"),
            new CopyWebpackPlugin([{
                from: 'src/assets/res',
                to: 'assets'
            }])
            // ,new webpack.optimize.CommonsChunkPlugin({
            //     name: 'common' // Specify the common bundle's name.
            // })
        ],
        devServer: {
            compress: true,
            port: 8080,
            historyApiFallback: true
        }
    }
};