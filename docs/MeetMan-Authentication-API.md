# MeetMan Authentication APIs List

`End Point:` <http://meetmanapi.demo.malekmakan.com/api>

## 1. Signup
`/signup` __POST__

Register user in an unconfirmed state and will send an email containing the Verification URL

### Accepts: 
```
{
    emailAddress,
    password,
    firstName,
    lastName,
    languageCodeValue
}
```

### Responses: 
200 (OK) or Bad Request with error message.
_____

## 2. External Signup
`/external` __POST__

Register user in confirmed state via a third party login.

### Accepts: 
```
{
    emailAddress, 
    loginProvider, // e.g. Facebook, Google, Live
    providerKey, 
    name // user's name 
}
```

### Responses: 
200 (OK) or Bad Request with error message.
_____

## 3. External Login
`/account/externallogin/{provider}/[error]` __GET__

Login via a third party provider. 

* `error` is not mandatory

### Responses: 
200 (OK) or Bad Request with error message.
_____

## 4. Get Local Access Token
`/account/getlocalaccesstoken/{provider}/{externalAccessToken}` __GET__

Returns the local access token needed for further transactions.

### Responses: 
200 (OK) Access Token Response
_____

## 5. Validate Username
`/account/checkusername` __POST__

It Checks whether the username is already taken.

### Accepts: 
```
{
    emailAddress
}
```

### Responses:
200 (OK) in case this username does not exists in database.
_____

## 6. Recover Account 
`/account/recoveraccount` __POST__

Will send the account recovery email.

### Accepts: 
```
{
    emailAddress
}
```

### Responses:
200 (OK) in case of success.
_____

## 7. Confirm Reset 
`/account/confirmreset` __POST__

Will confirm the password reset request based on the token that has been sent to user via email.

### Accepts: 
```
{
    userId,
    token,
    newPassword
}
```

### Responses:
200 (OK) in case of success.
_____

## 8. Confirm Email 
`/account/confirmemail` __POST__

Will confirm newly registered user's email address based on the token sent to user.

### Accepts: 
```
{
    userId,
    token
}
```

### Responses:
200 (OK) in case of success.
_____

## 9. Change Password 
`/account/changepassword` __POST__

Changes the user's password.

### Accepts: 
```
{
    userId,
    currentPassword,
    newPassword
}
```

### Responses:
200 (OK) including `Succeeded` field which is referring to success or failure of operation.
_____

## 10. Authenticate 
`/token/` __POST__

Authenticate user and return a token for further transactions.

### Accepts: 
```
grant_type=password&username={username which is email address}&password={password}
```

### Responses:
200 (OK) 
```
{
    access_toke: token hash,    
    token_typ: bearer,
    expires_in: token life time in ms,
    userName: username,
    userId: a GUID which is user id in database,
    .issued: token issue date time,
    .expires: token expiry date time
}
```
_____

## Best shape of Requests

```
    url: url,
    type: 'POST',
    headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With'
            },
    data: data
```

