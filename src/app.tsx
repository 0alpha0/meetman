import { h, render } from 'preact';
import Router from './router'
import './styles/meetman.scss';

render(<Router />, document.querySelector('#app')!);