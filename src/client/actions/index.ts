export const bubSwitch = (bubId) => {
  return {
    type: 'BUBBLE',
    bubId
  };
};

export const gridSwitch = () => {
  return {
    type: "GRID"
  }
}

export const bookSlot = (slotId) => {
  return {
    type: "SLOT",
    slotId
  }
}

export const confirmSlot = (slotId) => {
  return {
    type: "CONFIRM",
    slotId
  }
}

export const done = () => {
  return {
    type: "DONE"
  }
}