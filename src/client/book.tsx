import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import reducers from '../reducers';
import { bindActions } from '../common/util';
import { bookSlot, confirmSlot } from './actions'

@connect(reducers, bindActions({ bookSlot, confirmSlot }))
export default class Booking extends Component<any, any> {

    slotbutton = (val) => {
        const { bookSlot, client, confirmSlot } = this.props
        let option = client.slots[val] && client.slots[val].selected || false
        let confirmed = client.slots[val] && client.slots[val].confirmed || ""
        return (
            <div>
                {
                    !option &&
                    <a href="#" onClick={() => bookSlot(val)}>
                        <div className={`book-slot${confirmed && " selected"}`}>
                            <span>{val} am</span>
                        </div>
                    </a>
                }
                {
                    option &&
                    <div className="yes-no">
                        <a href="#" onClick={() => confirmSlot(val)}>
                            <img className="float-left" src="/assets/img/ok.png" alt="OK" />
                        </a>
                        <a href="#" onClick={() => bookSlot(val)}>
                            <img className="float-right" src="/assets/img/cancel.png" alt="Cancel" />
                        </a>
                    </div>
                }
            </div>
        )
    }

    slotsMap = (data: Array<any>) => {
        let slots: any = []
        data.map(val => {
            let slot = (
                <div className="col-3">
                    {this.slotbutton(val)}
                </div>
            )
            slots.push(slot);
        })
        return slots;
    }

    render() {
        return (
            <div className="row center book-row">
                {this.slotsMap(["9:30", "4:30", "6:80"])}
            </div>
        )
    }
}