import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import reducers from '../reducers';
import { bindActions } from '../common/util';
import { done } from './actions'


@connect(reducers, bindActions({ done }))
export default class Confirm extends Component<any, any> {

    render({ client, done }) {
        return (
            <div>
                <div className="row center book-info">
                    <div className="col-6">
                        <input name="date" type="text" value={client.postData['time']} readOnly />
                    </div>
                    <div className="col-6">
                        <input name="name" type="text" placeholder="Name" required />
                    </div>
                    <div className="col-6">
                        <input name="email" type="email" placeholder="Email" required />
                    </div>
                    <div className="col-6">
                        <input name="hp" type="text" placeholder="Phone" />
                    </div>
                </div>
                <div className="row center">
                    <div className="col-12">
                        <a href="#" onClick={() => done()}>
                            <div className="book-slot float-right">
                                <span>Confirm</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}