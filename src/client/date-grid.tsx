import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import reducers from '../reducers';
import { bindActions } from '../common/util';
import { bubSwitch, gridSwitch } from './actions'

export interface CalendarProps {
    i18n: object
    client: any
    bubSwitch: typeof bubSwitch
    gridSwitch: typeof gridSwitch
}

@connect(reducers, bindActions({ bubSwitch, gridSwitch }))
export default class CalendarComponent extends Component<any, any> {
    dayBubble = (date) => {
        const { client, bubSwitch } = this.props
        let selected = client.bubbles[date] && client.bubbles[date].selected || ""
        let open = client.bubbles[date] && client.bubbles[date].open || false
        let appointments = client.bubbles[date] && client.bubbles[date].data
        return (
            <div className="col-1">
                <a href="#" onClick={() => bubSwitch(date)}>
                    <span className={`date-bubble${selected && " selected"}`}>
                        <span>{date}</span>
                    </span>
                    {
                        open &&
                        <span className="date-info">
                            <span>{appointments}</span>
                        </span>
                    }
                </a>
            </div>
        )
    }
    weekRow = (...kids) => {
        const { client } = this.props;
        return (
            <div className="row center week-grid">
                <div className="col-1">
                    {client.week &&
                        <img className="week-arrow" src="/assets/img/left-arrow.png" alt="left-arrow" />}
                </div>
                {...kids}
                <div className="col-1">
                    {client.week &&
                        <img className="week-arrow" src="/assets/img/right-arrow.png" alt="right-arrow" />}
                </div>
            </div>
        )
    }

    weekFactory = (dates: Array<any>) => {
        const { client } = this.props;
        let week: any = [];
        do {
            let days: any = [];
            dates.splice(0, 7).map((val, idx) => {
                let bubble = this.dayBubble(val)
                days.push(bubble)
            })
            week.push(this.weekRow(days))
            if (client.week) break;
        } while (dates.length > 0)
        return week;
    }

    render({ i18n, client, gridSwitch }: CalendarProps) {
        return (
            <div>
                <div className="row">
                    <div className="col-12">
                        <div className="center">
                            <img className={`month-arrow ${client.week && "hide"}`} src="/assets/img/left-arrow.png" alt="left-arrow" />
                            <h3 className="today">June</h3>
                            <img className={`month-arrow ${client.week && "hide"}`} src="/assets/img/right-arrow.png" alt="right-arrow" />
                            <span className="icon-container">
                                <div onClick={() => gridSwitch()}>
                                    <i className="week-icon"></i>
                                    <img className="grid-icon" src="/assets/img/grid.png" alt="grid" />
                                </div>
                            </span>
                        </div>
                    </div>
                </div>

                <div className="row center week-grid">
                    <div className="col-1"></div>
                    <div className="col-1">F</div>
                    <div className="col-1">S</div>
                    <div className="col-1">S</div>
                    <div className="col-1">M</div>
                    <div className="col-1">T</div>
                    <div className="col-1">W</div>
                    <div className="col-1">T</div>
                    <div className="col-1"></div>
                </div>
                {
                    this.weekFactory([8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21])
                }
            </div>
        )
    }
}