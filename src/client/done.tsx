import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import reducers from '../reducers';


@connect(reducers)
export default class Done extends Component<any, any> {

    render() {
        return (
            <div className="book-container">
                <div className="row center">
                    <div className="col-12">
                        <div className="selected confirmed">
                            <span>16 Jun, 9 am to 10 am confirmed</span>
                        </div>
                    </div>
                </div>
                <div className="row center done">
                    <div className="col-4">
                        <div className="btn">
                            <span>Book another</span>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="btn">
                            <span>Change it</span>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="btn">
                            <span>Cancel it</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}