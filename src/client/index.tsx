import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import reducers from '../reducers';
import { bindActions } from '../common/util';
import { switchLang } from '../i18n/actions';

/**
 * COMPONENTS
 */
import DateGrid from './date-grid'
import Tabs from './tabs'
import Book from './book'
import Done from './done'
import Confirm from './confirm'

@connect(reducers, bindActions({ switchLang }))
export default class HomeComponent extends Component<any, any> {

    render({ client }) {
        return (
            <div className="header">
                <div className="container">
                    {client.steps.init && <Tabs />}
                    {client.steps.init && <DateGrid />}
                    <div className="book-container">
                        {client.steps.booking && <Book />}
                        {client.steps.confirm && <Confirm />}
                        {client.steps.done && <Done />}
                    </div>
                </div>
            </div>
        )
    }
}