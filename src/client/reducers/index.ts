interface State {
    bubbles: object
    slots: object
    week: boolean
    previous: Array<any>
    steps: { init: boolean, booking: boolean, confirm: boolean, done: boolean }
    postData: object
}

interface Action {
    type: string
    bubId: any
    slotId: any
}

export const client = (
    state: State = {
        bubbles: {},
        previous: [],
        week: true,
        slots: {},
        postData: {},
        steps: { init: true, booking: false, confirm: false, done: false }
    },
    action: Action
) => {
    switch (action.type) {
        case "BUBBLE": { //TBO
            console.log(action.bubId, state.bubbles)
            if (state.bubbles[action.bubId] && state.bubbles[action.bubId].selected) {
                return state
            }
            //if defined and open, select it & fetch slot data
            if (state.bubbles[action.bubId] && state.bubbles[action.bubId].open) {
                state.bubbles[action.bubId].selected = state.steps.booking = true
            }
            //reset all bubbles and find selected
            for (let obj in state.bubbles) {
                if (state.bubbles[action.bubId] && state.bubbles[action.bubId].selected) continue; //close all except selected
                state.bubbles[obj].open = state.bubbles[obj].selected = state.steps.booking = false
            }
            //fetch data for the bubble and open it
            if (typeof state.bubbles[action.bubId] === "undefined") {
                state.bubbles[action.bubId] = { open: true, selected: false, data: (Math.floor(Math.random() * (10 - 0 + 1)) + 0) }
            }
            //if it's already open close it
            else {
                state.bubbles[action.bubId].open = !state.bubbles[action.bubId].open
            }
            return {
                ...state,
                bubbles: state.bubbles
            }
        }
        case "GRID": {
            return {
                ...state,
                week: !state.week
            }
        }
        case "SLOT": {
            if (state.steps.confirm) return state;
            //reset all slots except the selected one
            for (let obj in state.slots) {
                if (state.slots[action.slotId] && state.slots[action.slotId].selected) continue
                state.slots[obj].selected = false
            }
            let cases = state.slots[action.slotId] && state.slots[action.slotId].selected || undefined
            switch (cases) {
                case true: {
                    state.slots[action.slotId].selected = false
                    break
                }
                case false: {
                    state.slots[action.slotId].selected = true
                    break
                }
                default: {
                    state.slots[action.slotId] = { selected: true, confirmed: false }
                }
            }
            return {
                ...state,
                slots: state.slots
            }
        }
        case "CONFIRM": {
            state.slots[action.slotId] = { selected: false, confirmed: true }
            state.steps.confirm = true
            state.postData["time"] = action.slotId
            return {
                ...state,
                steps: state.steps,
                postData: state.postData
            }
        }
        case "DONE": {
            for (let step in state.steps) {
                state.steps[step] = false
            }
            state.steps.done = true
            return {
                ...state,
                steps: state.steps
            }
        }
        default:
            return state
    }
}