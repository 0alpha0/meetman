import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import reducers from '../reducers';

export interface TabProps {
    i18n: object
}

@connect(reducers)
export default class Tabs extends Component<any, any> {

    render({ i18n }: TabProps) {
        return (
            <div>
                <div className="row">
                    <div className="col-4">
                        <div className="tab"><span className="badge">1</span> <span className="pill-text">{i18n['pickaday']}</span> </div>
                    </div>
                    <div className="col-4">
                        <div className="tab"><span className="badge">2</span> <span className="pill-text">{i18n['pickatime']}</span> </div>
                    </div>
                    <div className="col-4">
                        <div className="tab"><span className="badge">3</span> <span className="pill-text">{i18n['confirm']}</span> </div>
                    </div>
                </div>
            </div>
        )
    }
}