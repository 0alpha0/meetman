import { h, Component } from 'preact';

export interface ErrorProps {
    error?: string,
    url?: string
}

export default class ErrorComponent extends Component<any, any> {
    render({ error, url }: ErrorProps) {
        return <p>you've been hit by an error {error} on link {url}</p>
    }
}