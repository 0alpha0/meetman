export const switchLang = (i18n) => {
    return {
      type: 'SWITCH',
      i18n
    };
  };