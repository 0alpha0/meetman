import lang from '../'

// interface State { //use to verify language statments once all complete
//     say: object;
// }

interface Action {
    type: string;
    i18n: string;
}
const i18n = (state: object = lang["en"], action: Action) => {
    switch (action.type) {
        case 'SWITCH':
            state = lang[action.i18n]
            return { ...state }
        default:
            return state;
    }
};

export default i18n;