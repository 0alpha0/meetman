import { combineReducers } from 'redux';
import { client } from './client/reducers';
import i18n from './i18n/reducers';

const reducers = combineReducers({
  client,
  i18n
});

export default reducers;
