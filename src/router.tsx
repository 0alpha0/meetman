import { h, FunctionalComponent } from 'preact';
import { Router, RouterProps } from 'preact-router'
import { Provider } from 'preact-redux';
import { createStore } from 'redux';
import reducers from './reducers';

import Home from './client';
import E404 from './error/404';

const store = createStore(reducers);

const AppRouter: FunctionalComponent<RouterProps> = () => (
    <Provider store={store}>
        <Router>
            <Home path="/" />
            <E404 default error="none" />
        </Router>
    </Provider>
);

export default AppRouter